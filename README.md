# README

This workspace is using yarn 2 so installing dependencies should be instant thanks to yarn cache (committed into repo -> .yarn/cache).
https://yarnpkg.com/getting-started/install
https://yarnpkg.com/getting-started/migration
https://yarnpkg.com/cli/plugin/list

# Get started

`yarn workspace @stash/stash-app start`
