import { IntlProvider } from "react-intl";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import messageEnUs from "./lang/en-US.json";
import messageEnAu from "./lang/en-AU.json";
import { Home } from "./Pages/Home";

const messages = {
  "en-US": messageEnUs,
  "en-AU": messageEnAu,
  "en-NZ": messageEnAu,
};
const language = process.env.REACT_APP_LANG || navigator.language || "en-AU"; // language without region code

function App() {
  return (
    <IntlProvider
      locale={language}
      messages={messages[language]}
      defaultLocale="en-AU"
    >
      <Router basename={process.env.PUBLIC_URL}>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </IntlProvider>
  );
}

export default App;
