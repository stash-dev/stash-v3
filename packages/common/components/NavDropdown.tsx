import * as React from "react";
import { Menu, Transition } from "@headlessui/react";

type List = { disabled?: boolean; label: string }[];

export const NavDropdown: React.FC<{ list: List }> = ({ list, children }) => {
  return (
    <div className="flex items-center justify-center">
      <div className="relative z-1000 inline-block text-left">
        <Menu>
          {({ open }) => (
            <>
              <span className="rounded-md shadow-sm">{children}</span>
              <Transition
                show={open}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0"
              >
                <Menu.Items
                  static
                  className="absolute mt-3 right-0 w-56origin-top-right bg-white border border-gray-200 divide-y divide-gray-100 shadow-lg outline-none"
                >
                  {list.map((item) => (
                    <Menu.Item
                      as={(item.disabled && "span") || "a"}
                      disabled={item.disabled}
                    >
                      {({ active }) => (
                        <a
                          href="#account-settings"
                          className={`${
                            active
                              ? "bg-gray-100 text-gray-900"
                              : "text-gray-700"
                          } flex justify-between w-full px-4 py-2 text-sm leading-5 text-left`}
                        >
                          {item.label}
                        </a>
                      )}
                    </Menu.Item>
                  ))}
                </Menu.Items>
              </Transition>
            </>
          )}
        </Menu>
      </div>
    </div>
  );
};
